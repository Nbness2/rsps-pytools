class NotEqualsError(BaseException):
    pass


class NotContainedInError(BaseException):
    pass


class NotLessThanError(BaseException):
    pass


class NotGreaterThanError(BaseException):
    pass


class MaxLenBrokenError(BaseException):
    pass


def advanced_input(*, message, field, expected_datatype=str, mustconfirm=False,
                   mustequal=None, mustbein=None, lessthan=None, greaterthan=None, maxlen=None):
    unconfirmed = True
    if mustconfirm:
        confirm_message = "Confirm value \"{}\" for ({} \"{}\") (y/n): "

    if expected_datatype is not None:
        typename = repr(expected_datatype).split("'")[1]
    while True:
        try:
            data = input(message)
            if mustconfirm:
                if input(confirm_message.format(data, typename, field))[0].lower() == "y":
                    pass
                else:
                    continue
            if expected_datatype is not None:
                data = expected_datatype(data)

            if mustequal is not None:
                if data == mustequal:
                    return data
                else:
                    raise NotEqualsError()

            if mustbein is not None:
                if data in mustbein:
                    return data
                else:
                    raise NotContainedInError()

            if lessthan is not None:
                if not data < lessthan:
                    raise NotLessThanError()

            if greaterthan is not None:
                if not data > greaterthan:
                    raise NotGreaterThanError()

            if maxlen and maxlen > 0 and maxlen is not None:
                if not len(data) <= maxlen:
                    raise MaxLenBrokenError()

            return data

        except IndexError:
            print("You must put \"y\" or \"n\" in for an answer")
        except (TypeError, ValueError):
            print("You must put in a proper value for the type {}, not \"{}\"".format(typename, data))
        except NotEqualsError:
            print("Your input must equal {}".format(mustequal))
        except NotContainedInError:
            print("Your input must be contained in {}".format(mustbein))
        except NotLessThanError:
            print("Your input must be less than {}".format(lessthan))
        except NotGreaterThanError:
            print("Your input must be greater than {}".format(greaterthan))
        except MaxLenBrokenError:
            print("The length of your input must be less than or equal to {}".format(maxlen))