import ast
safe_eval = ast.literal_eval


class BadDefaultConfig(BaseException):
    pass


def read_config(plugin_name, default_configs, default_config_order=None):
    configs = {}
    try:
        with open("Configs/{}_config.ini".format(plugin_name)) as configfile:
            configlines = configfile.readlines()

        for line in configlines:
            line = line.split("=")
            configs[line[0].strip()] = safe_eval(line[1].strip())
        return configs
    except FileNotFoundError:
        write_config(plugin_name, configs=default_configs, order=default_config_order)
    try:
        return read_config(plugin_name, default_configs, default_config_order)
    except:
        raise BadDefaultConfig("Failed to read config, bad default config given")


def write_config(plugin_name, *, configs, order=None, raise_error=True):
    with open("Configs/{}_config.ini".format(plugin_name), "w+") as configfile:
        if order:
            try:
                for field in order:
                    if type(configs[field]) == str and "/" in configs[field]:  # Anything with an fslash in the field will be considered to be a filepath
                        configfile.write("{}='Plugins/{}/{}'\n".format(field, plugin_name, configs[field]))
                    else:
                        configfile.write("{}={}\n".format(field, repr(configs[field])))
            except KeyError as e:
                if raise_error:
                    raise e
                else:
                    pass
        else:
            for field, val in configs.items():
                print(field, val)
                if type(val) == str and "/" in val:
                    configfile.write("{}='Plugins/{}/{}'\n".format(field, plugin_name, val))
                else:
                    configfile.write("{}={}\n".format(field, repr(val)))
