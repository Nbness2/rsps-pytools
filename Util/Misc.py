
def del_fields_from_entries(input_dict, *, fieldnames):
    for dict_entry in input_dict.keys():
        for field_name in fieldnames:
            input_dict[dict_entry].pop(field_name)


def add_from_dict_to_dict(db1, db2, field):
    for item in db2:
        db1[item][field] = db2[item][field]
    return db1


def generate_options_message(*, option_message, option_names, victim_name, appended_message=None, append_newline=False):
    option_message = [
        option_message.format(opt_id+1, opt_name, victim_name) for opt_id, opt_name in enumerate(option_names)
        ]
    ret_msg = "{}{}{}".format(
        "\n".join(option_message),
        "\n" if append_newline else "",
        appended_message if appended_message else ""
    )
    return ret_msg


if __name__ == '__main__':
    #  This example is equal to the item editor's output.
    optmsg = "[{}]. Edit {} of item \"{}\""
    optnames = ("item name", "item description", "item bonuses", "item price", "loan data")
    victname = "Dragon Claws"
    print(
        generate_options_message(
            option_message=optmsg,
            option_names=optnames,
            victim_name=victname,
            append_newline=True,
            appended_message="Select an option: "
        )
    )