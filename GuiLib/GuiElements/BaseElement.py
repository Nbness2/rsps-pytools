class BaseElement:
    def __init__(self,
                 title, text, row=None, column=0, colspan=0, rowspan=0, bg_color=None, fg_color=None, font=None, hidden=False
                 ):
        self.title = title
        self.text = text
        self.row = row
        self.column = column
        self.colspan = colspan
        self.rowspan = rowspan
        self.bg_color = bg_color
        self.fg_color = fg_color
        self.font = font
        self.hidden = hidden


class BaseSubwidget:
    def __init__(
            self, subwidget_name, title=None, *,
            labels=None, buttons=None, tick_boxes=None, option_boxes=None, spin_boxes=None, list_boxes=None,
            scales=None, text_areas=None, entry_boxes=None, meters=None, property_boxes=None, separators=None,
            weblinks=None, window_grips=None, date_pickers=None, messages=None, modal=True, resizable=True
            ):

        self.subwidget_name = subwidget_name
        self.title = title
        self.modal = modal
        self.resizable = resizable
        self.labels = labels
        self.buttons = buttons
        self.tick_boxes = tick_boxes
        self.option_boxes = option_boxes
        self.spin_boxes = spin_boxes
        self.list_boxes = list_boxes
        self.scales = scales
        self.text_areas = text_areas
        self.entry_boxes = entry_boxes
        self.meters = meters
        self.property_boxes = property_boxes
        self.separators = separators
        self.weblinks = weblinks
        self.window_grips = window_grips
        self.date_pickers = date_pickers
        self.messages = messages


class Label(BaseElement):
    def __init__(self,
                 label_title, label_text, row=None, column=0, colspan=0, rowspan=0, hidden=False, *,
                 bg_color=None, fg_color=None, font=None, flashing=False
                 ):
        BaseElement.__init__(self, label_title, label_text, row, column, colspan, rowspan, bg_color, fg_color, font, hidden)
        self.flashing = flashing


class Button(BaseElement):
    def __init__(self,
                 button_title, button_text, row=None, column=0, colspan=0, rowspan=0, hidden=False, *,
                 bg_color=None, fg_color=None, button_function=None, font=None, image_file=None
                 ):
        BaseElement.__init__(self, button_title, button_text, row, column, colspan, rowspan, bg_color, fg_color, font, hidden)
        self.image_file = None
        self.image_file = image_file
        self.button_function = button_function

    def on_press(self, data):  # perform something when pressed
        pass


class TickBox(BaseElement):
    #  tickbox_options and tickbox_functions must be the same length if tickbox_functions is given
    def __init__(self,
                 tickbox_title, row=None, column=0, colspan=0, rowspan=0, hidden=False, *,
                 font=None, tickbox_options=None, tickbox_functions=None, multiple_choice=False, default_ticked=[],
                 bg_color=None, fg_color=None
                 ):
        BaseElement.__init__(self, tickbox_title, None, row, column, colspan, rowspan, bg_color, fg_color, font, hidden)
        self.tickbox_options = tickbox_options  # The text that will be displayed next to each tick
        self.tickbox_functions = tickbox_functions  # Only available for Radio buttons
        self.multiple_choice = multiple_choice  # Tickboxes if True, Radio buttons if False
        self.default_ticked = default_ticked  # Only effective if multiple_choice is True. Iterable.


class OptionBox(BaseElement):
    #  optionbox_options and optionbox_functions must be the same length if tickbox_functions is given
    def __init__(self,
                 optionbox_title, row=None, column=0, colspan=0, rowspan=0, hidden=False, *,
                 font=None, optionbox_options=None, multiple_choice=False, default_ticked=None, bg_color=None,
                 fg_color=None
                 ):
        BaseElement.__init__(self, optionbox_title, None, row, column, colspan, rowspan, bg_color, fg_color, font, hidden)
        self.optionbox_options = optionbox_options  # The options that will be displayed, "separators" will can be entered as such: - Separatorname - and are not selectable
        self.multiple_choice = multiple_choice  # TickOptionBox if True else LabelOptionBox
        self.default_ticked = default_ticked  # Only effective if multiple_choice is True


class SpinBox(BaseElement):
    #  please dont use this lol spinbox is bad
    def __init__(self,
                 spinbox_title, row=None, column=0, colspan=0, rowspan=0, hidden=False, *,
                 font=None, spinbox_values=None, bg_color=None, fg_color=None
                 ):
        BaseElement.__init__(self, spinbox_title, None, row, column, colspan, rowspan, bg_color, fg_color, font, hidden)
        self.spinbox_values = spinbox_values


class ListBox(BaseElement):  # weird interaction with bg and fg colors
        def __init__(self,
                     listbox_title, row=None, column=0, colspan=0, rowspan=0, hidden=False, *,
                     bg_color=None, fg_color=None, listbox_values=[], font=None, multiple_choice=False,
                     display_rows=None, select_function=None
                     ):
            BaseElement.__init__(self, listbox_title, None, row, column, colspan, rowspan, bg_color, fg_color, font, hidden)
            self.multiple_choice = multiple_choice
            self.listbox_values = listbox_values
            self.select_function = select_function
            self.display_rows = display_rows


class Scale(BaseElement):
    def __init__(self,
                 scale_title, row=None, column=0, colspan=0, rowspan=0, hidden=False, *,
                 bg_color=None, fg_color=None, font=None, horizontal=True, show_val=True, min_val=0, max_val=100,
                 interval=25, increment_percentage=1, default_val=None, show_intervals=True, scale_function=None
                 ):
        BaseElement.__init__(self, scale_title, None, row, column, colspan, rowspan, bg_color, fg_color, font, hidden)
        self.horizontal = horizontal
        self.show_val = show_val
        self.min_val = min_val
        self.max_val = max_val
        self.interval = interval
        self.increment_percentage = increment_percentage
        if default_val:
            self.default_val = default_val
        else:
            self.default_val = (min_val+max_val)/2
        self.show_intervals = show_intervals
        self.scale_function = scale_function


class EntryBox(BaseElement):
    def __init__(self,
                 entrybox_title, row=None, column=0, colspan=0, rowspan=0, hidden=False, *,
                 bg_color=None, fg_color=None, font=None, entry_type="a", auto_entry_phrases=[], secret=False,
                 force_case=None, max_length=None, default_text=None, submit_function=None, change_function=None,
                 labeled=False
                 ):
        # entry types: ~ = all allowed, n = numeric,  a = auto, v = validation
        # force case: u = upper, l = lower, anything else will be no forced case
        BaseElement.__init__(self, entrybox_title, default_text, row, column, colspan, rowspan, bg_color, fg_color, font, hidden)
        self.entry_type = entry_type[0]
        if force_case and force_case[0] in 'ul':
            self.force_case = force_case[0]
        else:
            self.force_case = None
        self.auto_entry_phrases = auto_entry_phrases
        self.secret = secret
        self.max_length = max_length
        self.submit_function = submit_function
        self.change_function = change_function
        self.labeled = labeled


class TextArea(BaseElement):
    def __init__(self,
                 textarea_title, row=None, column=0, colspan=0, rowspan=0, hidden=False, *,
                 bg_color=None, fg_color=None, font=None, scrollable=False
                 ):
        BaseElement.__init__(self, textarea_title, None, row, column, colspan, rowspan, bg_color, fg_color, font, hidden)
        self.scrollable = scrollable


class Meter(BaseElement):
    def __init__(self,
                 meter_title, row=None, column=0, colspan=0, rowspan=0, hidden=False, *,
                 meter_text=None, main_meter_color="red", secondary_meter_color="green", meter_type="n",
                 default_meter_val=0, bg_color=None, fg_color=None, font=None
                 ):
        # meter types: n = normal left to right fill, s = split meter (1 color 2 values), d = dual meter (2 colors and values)
        BaseElement.__init__(self, meter_title, meter_text, row, column, colspan, rowspan, bg_color, fg_color, font, hidden)
        self.main_meter_color = main_meter_color
        self.secondary_meter_color = secondary_meter_color
        self.meter_type = meter_type
        self.default_meter_val = default_meter_val


class Message(BaseElement):
    def __init__(self,
                 message_title, row=None, column=None, colspan=0, rowspan=0, hidden=False, *,
                 message_text=None, bg_color=None, fg_color=None, font=None
                 ):
        BaseElement.__init__(self, message_title, message_text, row, column, colspan, rowspan, bg_color, fg_color, font, hidden)
