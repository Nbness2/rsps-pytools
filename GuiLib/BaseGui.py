from appJar import gui


class BaseWindow(gui):

    def __init__(
            self, title=None, geom=None, warn=True, debug=False, *,
            labels=None, buttons=None, tick_boxes=None, option_boxes=None, spin_boxes=None, list_boxes=None,
            scales=None, text_areas=None, entry_boxes=None, meters=None, property_boxes=None, separators=None,
            weblinks=None, window_grips=None, date_pickers=None, messages=None,
            default_bg_color="white", default_fg_color="black", default_font_size=16, default_font="Arial",
            resizable=True
            ):
        gui.__init__(self, title, geom, warn, debug)
        self.default_bg_color = default_bg_color
        self.default_fg_color = default_fg_color
        self.default_font_size = default_font_size
        self.default_font = default_font
        self.resizable = resizable
        self.labels = labels
        self.buttons = buttons
        self.tick_boxes = tick_boxes
        self.option_boxes = option_boxes
        self.spin_boxes = spin_boxes
        self.list_boxes = list_boxes
        self.scales = scales
        self.text_areas = text_areas
        self.entry_boxes = entry_boxes
        self.meters = meters
        self.property_boxes = property_boxes
        self.separators = separators
        self.weblinks = weblinks
        self.window_grips = window_grips
        self.date_pickers = date_pickers
        self.messages = messages
        self.setBg(self.default_bg_color)
        self.setFg(self.default_fg_color)
        self.setFont(default_font_size, default_font)
        self.init_labels(self.labels)
        self.init_buttons(self.buttons)
        self.init_entryboxes(self.entry_boxes)
        self.init_listboxes(self.list_boxes)
        self.init_scales(self.scales)
        self.init_optionboxes(self.option_boxes)
        self.init_spinboxes(self.scales)
        self.init_tickboxes(self.tick_boxes)
        self.init_textareas(self.text_areas)
        self.init_meters(self.meters)
        self.init_messages(self.messages)
        self.setResizable(self.resizable)

        if property_boxes:
            pass

        if separators:
            pass

        if weblinks:
            pass

        if window_grips:
            pass

        if date_pickers:
            pass

    def init_labels(self, label_list):
        if not label_list:
            return
        for label in label_list:
            if label.flashing:
                addfunc = self.addFlashLabel
            else:
                addfunc = self.addLabel
            addfunc(
                title=label.title,
                text=label.text,
                row=label.row,
                column=label.column,
                colspan=label.colspan,
                rowspan=label.rowspan
            )
            self.setLabelBg(
                label.title,
                label.bg_color
            )

            self.setLabelFg(
                label.title,
                label.fg_color
            )
            self.getLabelWidget(label.title).config(font=label.font)
            if label.hidden:
                self.hideLabel(label.title)

    def init_buttons(self, button_list):
        if not button_list:
            return
        for button in button_list:
            if button.image_file:
                self.addImageButton(
                    title=button.title,
                    func=button.button_function,
                    imgFile=button.image_file,
                    row=button.row,
                    column=button.column,
                    colspan=button.colspan,
                    rowspan=button.rowspan
                )
            else:
                self.addNamedButton(
                    name=button.text,
                    title=button.title,
                    func=button.button_function,
                    row=button.row,
                    column=button.column,
                    colspan=button.colspan,
                    rowspan=button.rowspan
                    )
            self.setButtonBg(
                button.title,
                button.bg_color
            )
            self.getButtonWidget(button.title).config(font=button.font)
            if button.hidden:
                self.hideButton(button.title)

    def init_tickboxes(self, tick_box_list):
        if not tick_box_list:
            return
        for tbox in tick_box_list:
            i = 0
            if tbox.multiple_choice:
                for tbox_option in tbox.tickbox_options:
                    self.addCheckBox(tbox_option, tbox.row+i, tbox.column, tbox.colspan, tbox.rowspan)
                    i += 1
                    if tbox_option in tbox.default_ticked:
                        self.setCheckBox(tbox_option, ticked=True)
                    self.getCheckBoxWidget(tbox_option).config(font=tbox.font)
                    self.setCheckBoxBg(
                        tbox_option,
                        tbox.bg_color
                    )
            else:
                for tbox_option in tbox.tickbox_options:
                    self.addRadioButton(tbox.title, tbox_option, tbox.row+i, tbox.column, tbox.colspan, tbox.rowspan)
                    i += 1
                self.setRadioTick(tbox.title, tick=True)
                for radio_button in self.getRadioButtonWidget(tbox.title):
                    radio_button.config(font=tbox.font)
                self.setRadioButtonBg(
                    tbox.title,
                    tbox.bg_color
                )
                self.setRadioButtonFg(
                    tbox.title,
                    tbox.fg_color
                )

    def init_optionboxes(self, option_box_list):
        if not option_box_list:
            return
        for obox in option_box_list:
            if obox.multiple_choice:
                self.addTickOptionBox(
                    title=obox.title,
                    row=obox.row,
                    column=obox.column,
                    colspan=obox.colspan,
                    rowspan=obox.rowspan,
                    options=obox.optionbox_options
                )
                for obox_option in obox.optionbox_options:
                    if obox_option in obox.default_ticked:
                        self.setOptionBox(obox.title, obox_option, True)
            else:
                self.addLabelOptionBox(
                    title=obox.title,
                    row=obox.row,
                    column=obox.column,
                    colspan=obox.colspan,
                    rowspan=obox.rowspan,
                    options=obox.optionbox_options
                )
            self.getOptionBoxWidget(obox.title).config(font=obox.font)
            self.setOptionBoxBg(
                obox.title,
                obox.bg_color
            )
            self.setOptionBoxFg(
                obox.title,
                obox.fg_color
            )
            if obox.hidden:
                self.hideOptionBox(obox.title)

    def init_spinboxes(self, spin_box_list):
        if not spin_box_list:
            return
        for sbox in spin_box_list:
            self.addSpinBox(
                title=sbox.title,
                row=sbox.row,
                column=sbox.column,
                colspan=sbox.colspan,
                rowspan=sbox.rowspan,
                values=sbox.spinbox_values
            )
            self.getSpinBoxWidget(sbox.title).config(font=sbox.font)
            self.setSpinBoxBg(
                sbox.title,
                sbox.bg_color
            )
            self.setSpinBoxFg(
                sbox.title,
                sbox.fg_color
            )
            if sbox.hidden:
                self.hideSpinBox(sbox.title)

    def init_listboxes(self, list_box_list):
        if not list_box_list:
            return
        for lbox in list_box_list:
            self.addListBox(
                name=lbox.title,
                row=lbox.row,
                column=lbox.column,
                colspan=lbox.colspan,
                rowspan=lbox.rowspan,
                values=lbox.listbox_values
            )
            self.setListBoxMulti(lbox.title, lbox.multiple_choice)
            self.setListBoxRows(lbox.title, lbox.display_rows)
            self.getListBoxWidget(lbox.title).config(font=lbox.font)
            for list_item in lbox.listbox_values:
                if lbox.bg_color:
                    self.setListItemBg(
                        lbox.title,
                        list_item,
                        lbox.bg_color
                    )
                if lbox.fg_color:
                    self.setListItemFg(
                        lbox.title,
                        list_item,
                        lbox.fg_color
                    )
            if lbox.hidden:
                self.hideListBox(lbox.title)
            self.setListBoxSubmitFunction(lbox.title, lbox.select_function)

    def init_scales(self, scale_list):
        if not scale_list:
            return
        for scale in scale_list:
            self.addLabelScale(
                title=scale.title,
                row=scale.row,
                column=scale.column,
                colspan=scale.colspan,
                rowspan=scale.rowspan
            )
            self.setScaleRange(
                title=scale.title,
                start=scale.min_val,
                end=scale.max_val,
                curr=scale.default_val
            )
            if scale.show_intervals:
                self.showScaleIntervals(
                    title=scale.title,
                    intervals=scale.interval
                )
            self.showScaleValue(scale.title, scale.show_val)
            self.setScaleIncrement(scale.title, scale.increment_percentage)
            self.setScaleChangeFunction(scale.title, scale.scale_function)
            self.setScaleHorizontal(scale.title) if scale.horizontal else self.setScaleVertical(scale.title)
            self.getScaleWidget(scale.title).config(font=scale.font)
            self.setScaleBg(
                scale.title,
                scale.bg_color
            )
            self.setScaleFg(
                scale.title,
                scale.fg_color
            )
            if scale.hidden:
                self.hideScale(scale.title)

    def init_entryboxes(self, entry_box_list):
        if not entry_box_list:
            return
        for ebox in entry_box_list:
            if ebox.entry_type == '~':
                if ebox.labeled:
                    self.addLabelEntry(
                        title=ebox.title,
                        row=ebox.row,
                        column=ebox.column,
                        colspan=ebox.colspan,
                        rowspan=ebox.rowspan,
                        secret=ebox.secret
                    )
                else:
                    self.addEntry(
                        title=ebox.title,
                        row=ebox.row,
                        column=ebox.column,
                        colspan=ebox.colspan,
                        rowspan=ebox.rowspan,
                        secret=ebox.secret
                    )
                if ebox.text:
                    self.setEntryDefault(ebox.title, ebox.text)
            elif ebox.entry_type == 'n':
                if ebox.labeled:
                    self.addLabelNumericEntry(
                        title=ebox.title,
                        row=ebox.row,
                        column=ebox.column,
                        colspan=ebox.colspan,
                        rowspan=ebox.rowspan,
                        secret=ebox.secret
                    )
                else:
                    self.addNumericEntry(
                        title=ebox.title,
                        row=ebox.row,
                        column=ebox.column,
                        colspan=ebox.colspan,
                        rowspan=ebox.rowspan,
                        secret=ebox.secret
                    )
                if ebox.text:
                    self.setEntryDefault(ebox.title, ebox.text)
            elif ebox.entry_type == 'a':
                if ebox.labeled:
                    self.addLabelAutoEntry(
                        title=ebox.title,
                        row=ebox.row,
                        column=ebox.column,
                        colspan=ebox.colspan,
                        rowspan=ebox.rowspan,
                        words=ebox.auto_entry_phrases,
                        secret=ebox.secret
                    )
                else:
                    self.addAutoEntry(
                        title=ebox.title,
                        row=ebox.row,
                        column=ebox.column,
                        colspan=ebox.colspan,
                        rowspan=ebox.rowspan,
                        words=ebox.auto_entry_phrases
                    )
                if ebox.text:
                    self.setEntryDefault(ebox.title, ebox.text)
            elif ebox.entry_type == 'v':
                if ebox.labeled:
                    self.addLabelValidationEntry(
                        title=ebox.title,
                        row=ebox.row,
                        column=ebox.column,
                        colspan=ebox.colspan,
                        rowspan=ebox.rowspan,
                        secret=ebox.secret
                    )
                else:
                    self.addValidationEntry(
                        title=ebox.title,
                        row=ebox.row,
                        column=ebox.column,
                        colspan=ebox.colspan,
                        rowspan=ebox.rowspan,
                        secret=ebox.secret
                    )
            if ebox.force_case == 'u':
                self.setEntryUpperCase(ebox.title)
            elif ebox.force_case == 'l':
                self.setEntryLowerCase(ebox.title)
            if ebox.max_length and type(ebox.max_length) == int:
                self.setEntryMaxLength(ebox.title, ebox.max_length)
            self.getEntryWidget(ebox.title).config(font=ebox.font)
            self.setEntryBg(
                ebox.title,
                ebox.bg_color
            )
            self.setEntryFg(
                ebox.title,
                ebox.bg_color
            )
            if ebox.hidden:
                self.hideEntry(ebox.title)
            if ebox.submit_function:
                self.setEntrySubmitFunction(ebox.title, ebox.submit_function)
            if ebox.change_function:
                self.setEntryChangeFunction(ebox.title, ebox.change_function)

    def init_textareas(self, text_area_list):
        if not text_area_list:
            return
        for text_area in text_area_list:
            if text_area.scrollable:
                self.addScrolledTextArea(
                    title=text_area.title,
                    row=text_area.row,
                    column=text_area.column,
                    colspan=text_area.colspan,
                    rowspan=text_area.rowspan
                )
            else:
                self.addTextArea(
                    title=text_area.title,
                    row=text_area.row,
                    column=text_area.column,
                    colspan=text_area.colspan,
                    rowspan=text_area.rowspan
                )
            self.getTextAreaWidget(text_area.title).config(font=text_area.font)
            self.setTextAreaBg(
                text_area.title,
                text_area.bg_color
            )
            self.setTextAreaFg(
                text_area.title,
                text_area.fg_color
            )
            if text_area.hidden:
                self.hideTextArea(text_area.title)

    def init_meters(self, meter_list):
        if not meter_list:
            return
        for meter in meter_list:
            if meter.meter_type == 'n':
                self.addMeter(
                    name=meter.title,
                    row=meter.row,
                    column=meter.column,
                    colspan=meter.colspan,
                    rowspan=meter.rowspan
                )
                self.setMeterFill(meter.title, meter.main_meter_color)
                self.setMeter(meter.title, meter.default_meter_val, meter.text)
            elif meter.meter_type == 's':
                self.addSplitMeter(
                    name=meter.title,
                    row=meter.row,
                    column=meter.column,
                    colspan=meter.colspan,
                    rowspan=meter.rowspan
                )
                self.setMeterFill(meter.title, [meter.main_meter_color, meter.secondary_meter_color])
                self.setMeter(meter.title, meter.default_meter_val, meter.text)
            elif meter.meter_type == 'd':
                self.addDualMeter(
                    name=meter.title,
                    row=meter.row,
                    column=meter.column,
                    colspan=meter.colspan,
                    rowspan=meter.rowspan
                )
                self.setMeterFill(meter.title, [meter.main_meter_color, meter.secondary_meter_color])
                self.setMeter(meter.title, [meter.default_meter_val, meter.default_meter_val], meter.text)
            self.setMeterBg(
                meter.title,
                meter.bg_color
            )
            self.setMeterFg(
                meter.title,
                meter.fg_color
            )
            if meter.hidden:
                self.hideMeter(meter.title)

    def init_messages(self, messages_list):
        if not messages_list:
            return
        for message in messages_list:
            self.addMessage(
                title=message.title,
                text=message.text,
                row=message.row,
                column=message.column,
                colspan=message.colspan,
                rowspan=message.rowspan
            )
            if message.hidden:
                self.hideMessage(message.title)


def make_font(font_name, px_size, bold=False, italic=False, underline=False):
    font = [font_name, px_size]
    if bold:
        font.append("bold")
    if italic:
        font.append("italic")
    if underline:
        font.append("underline")
    return font