from GuiLib import BaseGui
from GuiLib.GuiElements.BaseElement import *


def gui_test():
    font1 = BaseGui.make_font("Times", "23", True, True, True)
    font2 = BaseGui.make_font("Arial", "23", True, False, False)
    font3 = BaseGui.make_font("Fixedsys", "17", False, True, False)
    font4 = BaseGui.make_font("Wingbats", "5", False, False, False)
    font5 = BaseGui.make_font("Comic Sans", "19", False, False, True)

    def buttonPress(button):
        if button == "button_2":
            tgui.showButton("button_1")
            tgui.showEntry("entry_box_1")
        else:
            tgui.hide_all_listboxes()
            tgui.hide_all_labels()
            tgui.hide_all_buttons()
            tgui.hide_all_entryboxes()


    tlabels = [
        Label(
            "label_1", "label_1_text", 0, 0, 0, 1,
            bg_color="red",
            font=font1,
            flashing=False
            ),
        Label(
            "label_2", "label_2_text", 1, 1, 0, 1,
            bg_color="green",
            fg_color="orange",
            font=font2,
            flashing=True
            )
    ]

    tbuttons = [
        Button(
            "button_1", "button_1_txt", 0, 1, 0, 0,
            button_function=buttonPress,
            bg_color="blue",
            font=font3,
            hidden=True
        ),
        Button(
            "button_2", "button_2_txt", 1, 0, 0, 0,
            button_function=buttonPress,
            font=font3
        )

    ]

    tcheckboxes = [
        TickBox(
            "tick_box_1", 2, 0, 0, 1,
            tickbox_options=[
                "option1", "option2", "option3"
            ],
            multiple_choice=True,
            default_ticked=["option2"],
            font=font3,
            bg_color="purple"
        )
    ]

    toptionboxes = [
        OptionBox(
            "multi_choice_optionbox", 2, 1, 0, 0,
            optionbox_options=[
                '- Divider1 -', 'option1', 'option2',
                '- Divider2 -', 'option3', 'option4'
            ],
            multiple_choice=True,
            default_ticked=['option1', 'option4'],
            font=font1
        ),
        OptionBox(
            "single_choice_optionbox", 3, 1, 0, 0,
            optionbox_options=[
                '- Divider 1 -',
                'option1', 'option2',
                'option3', 'option4'
            ],
            multiple_choice=False,
            font=font2
        )
    ]

    tspinboxes = [
        SpinBox(
            'spinbox_1', 4, 1, 0, 0,
            spinbox_values=['stop', 'using', 'appjar', 'spinboxes'],
            font=font1
        )
    ]

    tlistboxes = [
        ListBox(
            'listbox_1', 5, 0, 0, 0,
            listbox_values=['list_val_1', 'list_val_2', 'list_val_3'],
            bg_color=None,
            fg_color=None
        )
    ]

    tscales = [
        Scale(
            'scale_1', 5, 1, 0, 0,
            horizontal=True,
            show_val=True,
            min_val=-50,
            max_val=50,
            interval=25,
            increment_percentage=5,
            default_val=0,
            scale_function=print,
            bg_color="#00ff00",
            fg_color="#ff00ff",
        )
    ]

    tentryboxes = [
        EntryBox(
            'entry_box_1', 6, 1, 0, 0,
            entry_type='a',
            font=font2,
            hidden=True
        ),
        EntryBox(
            'validation_box_1', 7, 1, 0, 0,
            entry_type='v',
            font=font2
        )
    ]

    ttextareas = [
        TextArea(
            'text_area_1', 7, 0, 0, 0,
            scrollable=False,
            bg_color="blue",
            fg_color="orange",
        )
    ]

    tmeters = [
        Meter(
            "meter_1", 8, 0, 0, 0,
            meter_text="meter_1_text",
            font=font4,
            default_meter_val=75,
            meter_type='s'
        )
    ]

    tgui = BaseGui.BaseWindow(
        resizable=False,
        labels=tlabels,
        buttons=tbuttons,
        tick_boxes=tcheckboxes,
        option_boxes=toptionboxes,
        spin_boxes=tspinboxes,
        list_boxes=tlistboxes,
        scales=tscales,
        entry_boxes=tentryboxes,
        text_areas=ttextareas,
        meters=tmeters
    )
    tgui.go()
