if __name__ == '__main__':
    from Plugins.Item_Editor import Item_Editor
    from Util import AdvancedInput
    choice = AdvancedInput.advanced_input(
        message="(G)UI editor or (C)LI editor: ",
        field="Choice",
        mustbein="GgCc"
    )
    if choice in "Gg":
        editor = Item_Editor.GuiEditor()
    else:
        editor = Item_Editor.CliEditor()
    editor.edit_items()
