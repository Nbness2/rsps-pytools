# author Nbness/nbness2 4/20/17

from Util import AdvancedInput, Misc, ConfigOps
from Plugins.Item_Editor import core
from Plugins.Item_Editor.itemdata import bonus_names
from GuiLib.BaseGui import BaseWindow as Window
from GuiLib.GuiElements import BaseElement as Element


default_config_order = [
    "item_read_path", "item_read_file",
    "item_write_path", "item_write_file",
    "replacenew"
]

default_configs = {
    "item_read_path": "itemdata/",
    "item_read_file": "item_list.json",
    "item_write_path": "itemdata/",
    "item_write_file": "item_list.json",
    "replacenew": False
}


class BaseEditor:

    def __init__(self):
        self.configs = ConfigOps.read_config(
            "Item_Editor",
            default_configs=default_configs,
            default_config_order=default_config_order
        )
        self.item_read_path = self.configs["item_read_path"]
        self.item_read_file = self.configs["item_read_file"]
        self.item_write_path = self.configs["item_write_path"]
        self.item_write_file = self.configs["item_write_file"]
        self.replace_new = self.configs["replacenew"]
        self.item_db = core.read_item_file(self.item_read_path, self.item_read_file)


class CliEditor(BaseEditor):

    @staticmethod
    def edit_item_name(item):
        new_name = AdvancedInput.advanced_input(
            message="New name for item (old is {}): ".format(item["Name"]),
            field="item name",
            mustconfirm=True
        )
        item["Name"] = new_name
        return item

    @staticmethod
    def edit_item_desc(item):

        new_desc = AdvancedInput.advanced_input(
            message="New description for item (old is {}): ".format(item["Description"]),
            field="item description",
            mustconfirm=True
        )

        item["Description"] = new_desc
        return item

    @staticmethod
    def edit_item_price(item):

        new_desc = AdvancedInput.advanced_input(
            message="New price for item (old is {}): ".format(item["Price"]),
            expected_datatype=int,
            field="item price",
            mustconfirm=True
        )

        item["Price"] = new_desc
        return item

    @staticmethod
    def edit_item_loan_data(item):

        new_desc = AdvancedInput.advanced_input(
            message="New loan id for item (old is {}): ".format(item["l"]),
            expected_datatype=int,
            field="loan item id",
            mustconfirm=True
        )

        item["l"] = new_desc
        return item

    @staticmethod
    def edit_item_bonuses(item):
        item_has_bonuses = bool(item.get("b"))
        new_bonus_msg = "New {} for item \"{}\" (old is {}): "
        if item_has_bonuses:
            bonuses = item["Bonuses"].copy()
        else:
            bonuses = [0 for _ in range(12)]
        for bonus_idx, bonus_val in enumerate(bonuses):
            bonus_name = bonus_names.bonus_names[bonus_idx]

            new_bonus_val = AdvancedInput.advanced_input(
                message=new_bonus_msg.format(bonus_name, item["Name"], bonuses[bonus_idx]),
                expected_datatype=int,
                field=bonus_name
            )

            bonuses[bonus_idx] = new_bonus_val
        item["Bonuses"] = bonuses
        return item

    def edit_items(self):

        edit_functs = (
            self.edit_item_name,
            self.edit_item_desc,
            self.edit_item_bonuses,
            self.edit_item_price,
            self.edit_item_loan_data
        )
        new_id = True
        max_id = max(self.item_db.keys())
        msg = {
            "get_id": "Enter the item id you wish to edit (min 1, max {}): ",
            "item_opt_msg": "[{}]. Edit {} of item \"{}\"",
            "opt_sel": "Select an option: ",
            "new_item": "Would you like to [S]tay on item {}, Move to [N]ew item or [Q]uit: "
        }
        options = ("item name", "item description", "item bonuses", "item price", "loan data")

        while True:

            if new_id:
                item_id = AdvancedInput.advanced_input(
                    message=msg["get_id"].format(max_id),
                    field="item id",
                    expected_datatype=int,
                    lessthan=max_id+1,
                    greaterthan=0
                )
                item = self.item_db[item_id].copy()
            item_name = item["Name"]
            options_message = Misc.generate_options_message(
                option_message=msg["item_opt_msg"],
                option_names=options,
                victim_name=item_name,
                append_newline=True,
                appended_message=msg["opt_sel"]
            )
            option_selected = AdvancedInput.advanced_input(
                message=options_message,
                field="option",
                expected_datatype=int,
                mustbein=range(1, len(options) + 1)
            )
            option_selected -= 1
            item = edit_functs[option_selected](item)
            self.item_db[item_id] = item.copy()

            option_selected = AdvancedInput.advanced_input(
                message=msg["new_item"].format(item["Name"]),
                field="option",
                mustbein=("n", "s", "q"))[0].lower()
            if option_selected == "n":
                new_id = True
            elif option_selected == "s":
                new_id = False
            else:
                core.write_items(self.item_db, self.item_write_path, self.item_write_file,  self.replace_new)
                break


class GuiEditor(BaseEditor):
    def __init__(self):
        BaseEditor.__init__(self)
        self.item_id_list_values = []
        for item_id in self.item_db:
            self.item_id_list_values.append("{} - {}".format(self.item_db[item_id]["Name"], item_id))

        self.main_labels = (
            Element.Label(
                label_title="item_list_text",
                row=0, column=0, colspan=0, rowspan=0,
                label_text="Item List"
            ),
            Element.Label(
                label_title="item_id_text",
                row=0, column=1, colspan=0, rowspan=0,
                label_text="Item Id:"
            ),
            Element.Label(
                label_title="item_desc_label",
                row=2, column=1, colspan=0, rowspan=0,
                label_text="Item Description"
            ),
            Element.Label(
                label_title="item_search_label",
                row=9, column=0, colspan=0, rowspan=0,
                label_text="Item Id Search"
            ),
            Element.Label(
                label_title="item_price_label",
                row=4, column=1, colspan=0, rowspan=0,
                label_text="Item Price"
            ),
            # item stat labels
            Element.Label(
                label_title="Stab Attack Label",
                row=0, column=2, colspan=0, rowspan=0,
                label_text="Stab Attack Bonus"
            ),
            Element.Label(
                label_title="Slash Attack Label",
                row=2, column=2, colspan=0, rowspan=0,
                label_text="Slash Attack Bonus"
            ),
            Element.Label(
                label_title="Crush Attack Label",
                row=4, column=2, colspan=0, rowspan=0,
                label_text="Crush Attack Bonus"
            ),
            Element.Label(
                label_title="Magic Attack Label",
                row=6, column=2, colspan=0, rowspan=0,
                label_text="Magic Attack Bonus"
            ),
            Element.Label(
                label_title="Ranged Attack Label",
                row=8, column=2, colspan=0, rowspan=0,
                label_text="Ranged Attack Bonus"
            ),
            Element.Label(
                label_title="Stab Defence Label",
                row=0, column=3, colspan=0, rowspan=0,
                label_text="Stab Defence Bonus"
            ),
            Element.Label(
                label_title="Slash Defence Label",
                row=2, column=3, colspan=0, rowspan=0,
                label_text="Slash Defence Bonus"
            ),
            Element.Label(
                label_title="Crush Defence Label",
                row=4, column=3, colspan=0, rowspan=0,
                label_text="Crush Defence Bonus"
            ),
            Element.Label(
                label_title="Magic Defence Label",
                row=6, column=3, colspan=0, rowspan=0,
                label_text="Magic Defence Bonus"
            ),
            Element.Label(
                label_title="Ranged Defence Label",
                row=8, column=3, colspan=0, rowspan=0,
                label_text="Ranged Defence Bonus"
            ),
            Element.Label(
                label_title="Strength Label",
                row=0, column=4, colspan=0, rowspan=0,
                label_text="Strength Bonus"
            ),
            Element.Label(
                label_title="Prayer Label",
                row=2, column=4, colspan=0, rowspan=0,
                label_text="Prayer Bonus"
            ),
        )

        self.main_entry_boxes = (
            Element.EntryBox(
                entrybox_title="Item Name",
                row=1, column=1, colspan=1,
                entry_type="~"
            ),
            Element.EntryBox(
                entrybox_title="Item Description",
                row=3, column=1, colspan=1,
                entry_type="~",
                max_length=112
            ),
            Element.EntryBox(
                entrybox_title="Item Price",
                row=5, column=1, colspan=1,
                entry_type="n",
            ),
            Element.EntryBox(
                entrybox_title="Item Id Search",
                row=10, column=0, colspan=1,
                entry_type="v",
                max_length=5,
                change_function=self.entry_change
            ),
            #  Bonus entry boxes
            Element.EntryBox(
                entrybox_title="Stab Attack Entry",
                row=1, column=2,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
            Element.EntryBox(
                entrybox_title="Slash Attack Entry",
                row=3, column=2,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
            Element.EntryBox(
                entrybox_title="Crush Attack Entry",
                row=5, column=2,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
            Element.EntryBox(
                entrybox_title="Magic Attack Entry",
                row=7, column=2,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
            Element.EntryBox(
                entrybox_title="Ranged Attack Entry",
                row=9, column=2,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
            Element.EntryBox(
                entrybox_title="Stab Defence Entry",
                row=1, column=3,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
            Element.EntryBox(
                entrybox_title="Slash Defence Entry",
                row=3, column=3,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
            Element.EntryBox(
                entrybox_title="Crush Defence Entry",
                row=5, column=3,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
            Element.EntryBox(
                entrybox_title="Magic Defence Entry",
                row=7, column=3,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
            Element.EntryBox(
                entrybox_title="Ranged Defence Entry",
                row=9, column=3,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
            Element.EntryBox(
                entrybox_title="Strength Entry",
                row=1, column=4,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
            Element.EntryBox(
                entrybox_title="Prayer Entry",
                row=3, column=4,
                entry_type="n",
                max_length=3,
                hidden=True
            ),
        )

        self.main_list_boxes = (
            Element.ListBox(
                listbox_title="item_id_list",
                row=1, column=0, colspan=0, rowspan=8,
                listbox_values=self.item_id_list_values,
                multiple_choice=False,
                select_function=self.list_click
            ),
        )

        self.main_buttons = (
            Element.Button(
                button_title="add_item_bonuses",
                button_text="Add Item Bonuses",
                button_function=self.button_press,
                row=8, column=1, colspan=0, rowspan=0
            ),
            Element.Button(
                button_title="remove_item_bonuses",
                button_text="Remove Item Bonuses",
                button_function=self.button_press,
                row=8, column=1, colspan=0, rowspan=0
            ),
            Element.Button(
                button_title="save_all_item_data",
                button_text="Save data to file",
                button_function=self.button_press,
                row=10, column=1, colspan=0, rowspan=0
            ),
        )

        self.main_window = Window(
            title="Itemdef editor - Nbness2",
            geom="500x500",
            labels=self.main_labels,
            list_boxes=self.main_list_boxes,
            buttons=self.main_buttons,
            entry_boxes=self.main_entry_boxes,
            default_font="Arial", default_font_size=12
        )
        self.main_window.setPollTime(250)
        self.item_search_is_valid = False
        self.current_item_id = 1
        self.main_window.addMenuList("Help", ["About"], self.menu_list_press)
        self.main_window.selectListItem("item_id_list", self.item_id_list_values[0])

    def edit_items(self):
        self.main_window.go()

    def button_press(self, button_title):
        functs = {
            "save_all_item_data": self.save_all_item_data,
            "add_item_bonuses": self.add_new_bonuses,
            "remove_item_bonuses": self.remove_item_bonuses
        }
        try:
            functs[button_title]()
        except KeyError:
            print("Unhandled button:", button_title)

    def menu_list_press(self, menu_button):
        functs = {
            "About": self.about_window

        }
        try:
            functs[menu_button]()
        except KeyError:
            print("Unhandled menu option:", menu_button)

    def list_click(self, list_title):
        functs = {
            "item_id_list": self.load_item_data
        }
        try:
            functs[list_title]()
        except KeyError:
            print("Unhandled list:", list_title)

    def entry_change(self, entry_title):
        functs = {
            "Item Id Search": self.validate_search_item_id,
        }
        try:
            functs[entry_title]()
        except KeyError:
            print("Unhandled entry change:", entry_title)

    def entry_submit(self, entry_title):
        functs = {
            "Item Id Search": self.search_item_id,
        }
        try:
            functs[entry_title]()
        except KeyError:
            print("Unhandled entry submit:", entry_title)

    def validate_search_item_id(self):
        value = self.main_window.getEntry("Item Id Search")
        if not value.isnumeric():
            self.main_window.setEntryInvalid("Item Id Search")
            self.item_search_is_valid = False
            return
        value = int(value)
        try:
            self.item_db[value]
        except KeyError:
            self.main_window.setEntryInvalid("Item Id Search")
            self.item_search_is_valid = False
            return
        except Exception as e:
            print("Unhandled exception:", e)
            self.main_window.setEntryInvalid("Item Id Search")
            self.item_search_is_valid = False
            return
        self.current_item_id = value
        self.main_window.setEntryValid("Item Id Search")
        self.item_search_is_valid = True
        self.search_item_id()

    def select_item_id(self, item_id):
            listed_name = "{} - {}".format(self.item_db[item_id]["Name"], item_id)
            self.main_window.selectListItem("item_id_list", listed_name, False)

    def search_item_id(self):
        if self.item_search_is_valid:
            item_id = int(self.main_window.getEntry("Item Id Search"))
            try:
                listed_name = "{} - {}".format(self.item_db[item_id]["Name"], item_id)
            except KeyError:
                self.main_window.warningBox(
                    title="Item Id Error",
                    message="Invalid item id (no data) {}".format(item_id)
                )
                return
            self.main_window.selectListItem("item_id_list", listed_name)
            self.current_item_id = item_id

    def hide_item_bonuses(self):
        self.main_window.hideButton("remove_item_bonuses")
        self.main_window.showButton("add_item_bonuses")
        for bonus_name in bonus_names.bonus_names:
            self.main_window.hideEntry("{} Entry".format(bonus_name))
            self.main_window.hideLabel("{} Label".format(bonus_name))

    def show_item_bonuses(self):
        self.main_window.hideButton("add_item_bonuses")
        self.main_window.showButton("remove_item_bonuses")
        for bonus_id, bonus_name in enumerate(bonus_names.bonus_names):
            self.main_window.showEntry("{} Entry".format(bonus_name))
            self.main_window.showLabel("{} Label".format(bonus_name))
            self.main_window.setEntry("{} Entry".format(bonus_name), self.current_item_data["Bonuses"][bonus_id])

    def add_new_bonuses(self):
        confirmed = self.main_window.yesNoBox("Add Item Bonus Confirmation", "Are you sure you want to add item bonuses to the item \"{}\" (id: {})".format(self.current_item_data["Name"], self.current_item_id))
        if confirmed:
            self.item_db[self.current_item_id]["Bonuses"] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            self.save_current_item_data()
            self.load_item_data()

    def remove_item_bonuses(self):
        confirmed = self.main_window.yesNoBox("Remove Item Bonus Confirmation", "Are you sure you want to remove item bonuses for the item \"{}\" (id: {})".format(self.current_item_data["Name"], self.current_item_id))
        if confirmed:
            del self.item_db[self.current_item_id]["Bonuses"]
            self.save_current_item_data()
            self.load_item_data()

    def get_item_bounses(self):
        if "Bonuses" in self.current_item_data:
            item_bonuses = []
            for bonus_id, bonus_name in enumerate(bonus_names.bonus_names):
                item_bonuses.append(int(self.main_window.getEntry("{} Entry".format(bonus_name))))
            return item_bonuses

    def load_item_data(self):  # loads the currently selected item id
        try:
            selected_id = int(self.main_window.getListItems("item_id_list")[0].split("-")[-1])
            if selected_id != self.current_item_id:
                self.save_current_item_data()
            self.select_item_id(selected_id)
            self.current_item_data = self.item_db[selected_id].copy()
            self.current_item_id = selected_id
            self.main_window.setLabel("item_id_text", "Item id: {}".format(selected_id))
            self.main_window.setEntry("Item Name", self.current_item_data["Name"], False)
            self.main_window.setEntry("Item Description", self.current_item_data["Description"], False)
            self.main_window.setEntry("Item Price", self.current_item_data["Price"], False)
            if "Bonuses" in self.current_item_data:
                self.main_window.setGeom("1200x500")
                self.show_item_bonuses()
                self.main_window.hideButton("add_item_bonuses")
            else:
                self.main_window.setGeom("500x500")
                self.hide_item_bonuses()
                self.main_window.showButton("add_item_bonuses")
        except IndexError:
            self.main_window.warningBox(
                title="Index Error",
                message="You did not pick an item to load!"
            )
            return
        except KeyError:
            self.main_window.warningBox(
                title="Item Id Error",
                message="Invalid item id {}".format(selected_id)
            )
            return

    def save_current_item_data(self):
        new_item_name = self.main_window.getEntry("Item Name")
        new_item_desc = self.main_window.getEntry("Item Description")
        new_item_price = int(self.main_window.getEntry("Item Price"))
        listed_name = "{} - {}".format(self.current_item_data["Name"], self.current_item_id)
        new_list_value = "{} - {}".format(new_item_name, self.current_item_id)
        idx = self.item_id_list_values.index(listed_name)
        self.item_id_list_values[idx] = "{} - {}".format(new_item_name, self.current_item_id)
        self.item_db[self.current_item_id]["Name"] = new_item_name
        self.item_db[self.current_item_id]["Description"] = new_item_desc
        self.item_db[self.current_item_id]["Price"] = int(new_item_price)
        if "Bonuses" in self.current_item_data:
            self.current_item_data["Bonuses"] = self.get_item_bounses()
            self.item_db[self.current_item_id]["Bonuses"] = self.current_item_data["Bonuses"]
        self.main_window.getListBoxWidget("item_id_list").delete(idx)
        self.main_window.getListBoxWidget("item_id_list").insert(idx, new_list_value)
        self.main_window.selectListItem("item_id_list", new_list_value)

    def save_all_item_data(self):
        self.save_current_item_data()
        core.write_items(self.item_db, self.item_write_path, self.item_write_file, self.replace_new)

    def about_window(self):
        self.main_window.infoBox("About Nbness2's itemdef editor", "Thank's for using this tool! You can check any updates and report any bugs at https://bitbucket.org/Nbness2/rsps-pytools/")
