import json


def write_items(item_db, item_write_path, item_write_file, replace_new):
        if not replace_new:
            from datetime import datetime
            now = datetime.now()
            nowtime = now.time()
            datetimestamp = "{}_{}_{}_{}_{}_{}".format(now.year, now.month, now.day,
                                                       nowtime.hour, nowtime.minute, nowtime.second
                                                       )
            item_write_file = "{}_{}".format(datetimestamp, item_write_file)
        with open(item_write_path+item_write_file, "w+") as newItemFile:
            newItemFile.write(json.dumps(item_db, separators=(',', ':')))


def get_item_list(item_read_path, item_read_file):
    with open(item_read_path+item_read_file, "r") as itemList:
        return itemList.readlines()


def read_item_file(item_read_path, item_read_file):
    with open(item_read_path+item_read_file) as itemJson:
        item_dict = json.loads(itemJson.read())
    item_dict = {int(iid): value for iid, value in item_dict.items()}
    return item_dict
