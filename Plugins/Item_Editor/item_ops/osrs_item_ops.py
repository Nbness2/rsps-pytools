import json


def read_osrs_item_file(item_read_path, item_read_file):
    item_read_file = item_read_file.split(".")[0]+".json"
    with open(item_read_path+item_read_file) as itemJson:
        item_dict = json.loads(itemJson.read())
    item_dict = {int(iid): value for iid, value in item_dict.items() if int(iid) > 11683}
    return item_dict
